package tp3;

public class Histoire {

	public static void main(String[] args) {
		Humain bob;
		bob= new Humain("bob");
		
		bob.sePresenter();
		bob.boire();
		
		Dame rachel;
		rachel= new Dame("Rachel");
		
		rachel.sePresenter();
		rachel.priseEnOtage();
		rachel.estLiberee();
		
		
		Brigand gunther;
		gunther= new Brigand("Gunther");
		
		gunther.sePresenter();
		
		Cowboy simon;
		simon= new Cowboy("Simon");
		
		simon.sePresenter();

	}

}
