package tp1;

public class Compte {

	//Attributs
	private int numero;
	private double decouvert, solde;

	//Constructeurs
	public Compte(int numero) {
		this.numero= numero;
		solde=0;
		decouvert= 0;

	}

	//Accesseurs
	void setDecouvert(double montant) {
		decouvert=montant;

	}
	double getDecouvert() {
		return decouvert;
	}

	int getNumero() {
		return numero;
	}

	double getSolde() {
		return solde;
	}

	//M�thodes
	void afficherSolde() {
		System.out.println("solde="+" "+getSolde());
	}
	void depot(double montant) {
		solde= solde + montant;

	}
	String retrait(double montant) {
		if(montant>decouvert+solde) {
			System.out.println("refuser");
		}
		else {
			solde-=montant;
			System.out.println("Accepter");
		}
		return null;



	}
}