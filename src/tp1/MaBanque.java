package tp1;

public class MaBanque {

	public static void main(String[] args) {
		Compte c;
		c= new Compte(1);

		c.getDecouvert();
		System.out.println("Decouvert="+" "+c.getDecouvert());

		c.setDecouvert(100);
		System.out.println("Nouveau decouvert="+" "+c.getDecouvert());

		c.afficherSolde();
		
		c.depot(10);
		c.afficherSolde();
		c.retrait(200);
		c.afficherSolde();
	}

}
