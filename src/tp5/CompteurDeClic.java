package tp6;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener{
	//Attributs
	int c;
	JButton clic = new JButton("Clic !") ;
	JLabel nombre = new JLabel("Vous avez cliqu� "+ " fois"); 

	//Constructeurs	
	public CompteurDeClic() {
		super();
		this.setTitle("Compteur de Bouton");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true); // Toujours � la fin du constructeur}


		Container panneau = getContentPane();
		panneau.add(clic);
		panneau.add(nombre);
		panneau.setLayout(new FlowLayout());
		clic.addActionListener(this);


	}
	//M�thodes main
	public static void main (String[] args) {
		CompteurDeClic app = new CompteurDeClic ();

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Une action a �t� detect�e");
		c=c+1;
		nombre.setText("Vous avez cliqu� "+c+ " fois");
	}
}
