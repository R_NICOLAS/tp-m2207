package tp2;

public class TestForme {

	public static void main(String[] args) {
		Forme f1, f2;
		f1= new Forme("orange",true);
		f2= new Forme("vert", false);



		System.out.println("f1:"+" "+f1.getCouleur()+" "+f1.isColoriage());
		System.out.println("f2:"+" "+f2.getCouleur()+" "+ f2.isColoriage());

		f1.setCouleur("rouge");
		f1.setColoriage(false);

		System.out.println("f1:"+" "+f1.getCouleur()+" "+f1.isColoriage());

		System.out.println("f1 est:"+" "+f1.seDecrire());
		System.out.println("f2 est:"+" "+f2.seDecrire());


		Cercle c1;
		c1= new Cercle();
		System.out.println("le cercle c1 � pour rayon"+" "+c1.getRayon());
		System.out.println("c1 est:"+" "+c1.seDecrire());

		Cercle c2;
		c2= new Cercle();

		c2.setRayon(2.5);
		System.out.println("Le rayon du cercle c2 est:"+" "+c2.getRayon());

		Cercle c3;
		c3= new Cercle();
		c3.setCouleur("jaune");
		c3.setColoriage(false);
		c3.setRayon(3.2);

		System.out.println("Le cercle c3:"+" "+c3.getCouleur()+" "+c3.getRayon()+" "+c3.isColoriage());
		
		
		System.out.println("aire de c3: "+c3.calculerAire());
		System.out.println("aire de c2: "+c2.calculerAire());
		System.out.println("perim�tre de c3: "+c3.calculerPerimetre());
		System.out.println("perim�tre de c2: "+c2.calculerPerimetre());
		
		Cylindre cy1, cy2;
		cy1= new Cylindre();
		cy2= new Cylindre();
		
		System.out.println("Le cylindre cy1:"+" "+cy1.getCouleur()+" "+cy1.getRayon()+" "+cy1.isColoriage());
		
		cy2.setCouleur("bleu");
		cy2.setColoriage(true);
		cy2.setRayon(1.3);
		cy2.setHauteur(4.2);
		
		System.out.println("cy2 est: "+cy2.seDecrire());
		System.out.println("volume cy2: "+cy2.calculerVolume());
		System.out.println("volume cy1: "+cy1.calculerVolume());
	}
	

}
