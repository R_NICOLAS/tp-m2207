package tp4;

public class Combat {

	public static void main(String[] args) {
		Pokemon guillaume, sarped;
		guillaume= new Pokemon("Guillaume");
		sarped= new Pokemon("Sarped");
		
		guillaume.sePresenter();
		sarped.sePresenter();
		
		sarped.attaquer(guillaume);
		guillaume.sePresenter();
		
		
		//Ne fonctionne pas (creation de la boucle pour les rounds)
		int c=0;
		while(guillaume.isAlive()==true){
			guillaume.sePresenter();
			sarped.sePresenter();
			sarped.attaquer(guillaume);
			c+=1;
		}
		System.out.println();

	}

}
