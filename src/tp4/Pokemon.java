package tp4;

public class Pokemon {
	//Attributs
	private int energie, maxEnergie, puissance;
	public String nom;
	


	//Constructeurs
	public Pokemon(String n) {
		nom=n;
		maxEnergie= 50+ (int)(Math.random()*((90-50)+1));
		energie= 30;
		puissance= 3+ (int)(Math.random()*((10-3)+1));
	}




	//Accesseurs
	String getNom() {
		return nom;
	}
	int getEnergie() {
		return energie;
	}
	int getPuissance() {
		return puissance;
	}
	//m�thodes
	void sePresenter() {
		System.out.println("Je suis "+nom+ " j'ai "+energie+" points d'energie( "+maxEnergie+" max) et une puissance de "+puissance);
	}
	void manger() {
		energie+= 10+ (int)(Math.random()*((30-10)+1));
		if(energie>maxEnergie) {
			energie=maxEnergie;
		}
		else if(energie <=0) {
			energie = 0;
		}
		System.out.println("Je suis "+nom+ " j'ai "+energie+" points d'energie( "+maxEnergie+" max)");

	}
	void vivre() {
		energie-= 20 +(int)(Math.random()*((40-20)+1));
		if (energie<=0) {
			energie= 0;
		}
		System.out.println("Je suis "+nom+ " j'ai "+energie+" points d'energie( "+maxEnergie+" max)");
	}
	boolean isAlive() {
		if(energie==0) {
			return false;
		}
		else {
			return true;
		}
	}
	void perdreEnergie(int perte) {
		energie-=perte;
		
	}
	void attaquer(Pokemon adversaire) {
		adversaire.energie-=puissance;
	}




}

